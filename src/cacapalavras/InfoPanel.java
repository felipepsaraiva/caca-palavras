
package cacapalavras;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.Timer;
import javax.swing.JProgressBar;
import javax.swing.Box.Filler;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InfoPanel extends JPanel {
    //<editor-fold defaultstate="collapsed" desc="Declaration">
    private final JLabel lbl1;
    private final JLabel lblScore;
    private final Filler filler;
    private final JLabel lbl2;
    private final JLabel lblTheme;
    private final JProgressBar timeBar;
    private final Timer timer;
    private final TimerListener timerListener;
    private int tempo;
    //</editor-fold>
    
    public InfoPanel() {
        setLayout(new FlowLayout());
        setBackground(Color.GRAY);
        tempo = 90; //Tempo inicial do Jogo
        timeBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, tempo);
        timeBar.setValue(timeBar.getMaximum());
        timerListener = new TimerListener();
        timer = new Timer(1000, timerListener);
        filler = new Filler(new Dimension(5, 0), new Dimension(10, 0), new Dimension(15, 0));
        //Iniciando Labels
        lbl1 = new JLabel("Tema:");
        lbl1.setForeground(Color.WHITE);
        lbl1.setFont(new Font("Tahoma", Font.BOLD, 15));
        lblTheme = new JLabel();
        lblTheme.setForeground(Color.WHITE);
        lblTheme.setFont(lbl1.getFont());
        
        lbl2 = new JLabel("Pontos:");
        lbl2.setForeground(Color.WHITE);
        lbl2.setFont(lbl1.getFont());
        lblScore = new JLabel("0");
        lblScore.setForeground(Color.WHITE);
        lblScore.setFont(lbl1.getFont());
        //Adicionando Componentes
        add(lbl1);
        add(lblTheme);
        add(filler);
        add(lbl2);
        add(lblScore);
        add(timeBar);
    }
    
    public void scoreAndReset() {
        int score = Integer.parseInt(lblScore.getText());
        score ++;
        lblScore.setText(String.format("%d", score));
        
        //Tornando o tempo disponível para achar a palavra menor e aplicando ele
        /*O Tempo decresce de 5 em 5 até os 30 segundos,
          A partir dai decresce de 2 em 2 ate os 10 segundos
          A partir dai decresce de 1 em 1 ate os 5 segundos, onde o tempo fica fixado*/
        if (tempo > 30)
            tempo -= 5;
        else if (tempo > 10)
            tempo -= 2;
        else if (tempo > 5)
            tempo -= 1;
        else if (tempo < 5)
            tempo = 5;
        
        timeBar.setMaximum(tempo);
        timeBar.setValue(timeBar.getMaximum());
    }
    
    public void setTheme(String texto) {
        lblTheme.setText(texto);
    }
    
    public String getScore() {
        return lblScore.getText();
    }
    
    public void stopTimer() {timer.stop();}
    public void startTimer() {timer.start();}
    
    public class TimerListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if(timeBar.getValue() == 0)
                Principal.getFrmWindow().gameOver();
            else
                timeBar.setValue(timeBar.getValue()-1);
        }
    }
}
