
package cacapalavras;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;

import java.util.Random;
import java.io.*;

public class FrmWindow extends JFrame {
    private GamePanel game;
    private final InfoPanel infos;
    private Word word;
    
    public FrmWindow(String titulo) {
        //Inicializando Frame
        super(titulo);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout(0, 5));
        setSize(600, 600);
        //Inicializando e Adicionando os Panels
        infos = new InfoPanel();
        word = new Word(selectWord());
        game = new GamePanel(word);
        
        add(infos, BorderLayout.PAGE_START);
        add(game, BorderLayout.CENTER);
        
        infos.startTimer();
    }
    
    protected void gameWon() {
        infos.stopTimer();
        infos.scoreAndReset();
        remove(game);
        word = new Word(selectWord());
        game = new GamePanel(word);
        add(game, BorderLayout.CENTER);
        infos.startTimer();
    }
    
    protected void gameOver() {
        infos.stopTimer();
        game.mostrarPalavra();
        JOptionPane.showMessageDialog(this, "O tempo acabou...\nVocê fez " + infos.getScore() + " pontos!");
        System.exit(0);
    }
    
    private String selectWord() {
        Random aleatorio = new Random();
        LineNumberReader entrada;
        try {
            File arquivo = selectFile();
            infos.setTheme(arquivo.getName().split(".txt")[0]);
            //Calculando o numero de linhas
            entrada = new LineNumberReader( new InputStreamReader(new FileInputStream(arquivo)) );
            entrada.skip(Long.MAX_VALUE);
            int linha = aleatorio.nextInt(entrada.getLineNumber() + 1);
            entrada.close();
            //Acessando a linha e criando a nova palavra
            entrada = new LineNumberReader( new InputStreamReader(new FileInputStream(arquivo), "ISO-8859-1") );
            int indexLinha = 0;
            while (indexLinha < linha) {
                entrada.readLine();
                indexLinha++;
            }
            String palavra = entrada.readLine();
            entrada.close();
            
            //Tratamento de Entradas Indevidas
            //Se a linha for nula, joga NullPointerException
            if (palavra == null)
                throw new NullPointerException("O Arquivo \"" + arquivo.getName() + "\" possui uma linha vazia!");
            //Se houver espaços na palavra, elimina os espaços
            if (palavra.contains(" "))
                palavra = palavra.replaceAll(" ", "");
            System.out.println(palavra); //////////////////////////////
            //Se a palavra for somente espaços, joga IllegalArgumentException
            if (palavra.equals(""))
                throw new IllegalArgumentException("O Arquivo \"" + arquivo.getName() + "\" possui uma linha vazia!");
            //Se a palavra for maior que o tabuleiro, joga IllegalArgumentException
            if ((palavra.length() > GamePanel.maxLinhas) || (palavra.length() > GamePanel.maxColunas))
                throw new IllegalArgumentException("A palavra selecionada (" + palavra + ") é muito grande!");
            //Se existir numeros na palavra, joga IllegalArgumentException
            for (int i=0 ; i<10 ; i++)
                if (palavra.contains(String.valueOf(i)))
                    throw new IllegalArgumentException("Não são permitidos números nas palavras!");
            return palavra.toUpperCase();
            
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(this, "Palavras\\" + e.getMessage());
            System.exit(1);
        } catch (IOException e1) {
            JOptionPane.showMessageDialog(this, e1);
            System.exit(1);
        } catch (NullPointerException | IllegalArgumentException e2) {
            JOptionPane.showMessageDialog(this, e2.getMessage());
            System.exit(1);
        }
        return null;
    }
    
    private File selectFile() throws FileNotFoundException, NullPointerException{
        Random aleatorio = new Random();
        File pasta = new File("Palavras");
        if (!pasta.exists())
            throw new NullPointerException("A pasta \"Palavras\" não existe!");
        String[] fileList = pasta.list();
        if (fileList.length == 0)
            throw new NullPointerException("A pasta \"Palavras\" está vazia!");
        int indiceArquivo = aleatorio.nextInt(fileList.length);
        File arquivo = new File("Palavras\\" + fileList[indiceArquivo]);
        if (arquivo.length() == 0)
            throw new NullPointerException("O arquivo \"" + arquivo.getName() + "\" está vazio!");
        return arquivo;
    }
    
}
