
package cacapalavras;

public class Principal {
    private static final FrmWindow application = new FrmWindow("Caça Palavras - Por Felipe Saraiva");
    
    public static void main(String[] args) {
        application.setVisible(true);
    }
    
    public static FrmWindow getFrmWindow() {
        return application;
    }
}
