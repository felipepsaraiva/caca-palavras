
package cacapalavras;
import java.util.Random;

public class Word {
    private final String palavra;
    private final int tamanho;
    private final int orientacao;
    
    public Word (String palavra) {
        this.palavra = removeAccentuation(palavra);
        this.tamanho = palavra.length();
        this.orientacao = new Random().nextInt(3);
    }
    
    @Override
    public String toString() {
        return palavra;
    }

    public int getTamanho() {
        return tamanho;
    }

    public int getOrientacao() {
        return orientacao;
    }
    
    public static String removeAccentuation(String texto) {
        char[] aux = texto.toCharArray();
        for (int i=0 ; i<aux.length ; i++) {
            switch (aux[i]) {
                case 'Ã': case 'Á': case 'À': case 'Â':
                    aux[i] = 'A';
                    break;
                    
                case 'É': case 'È': case 'Ê':
                    aux[i] = 'E';
                    break;
                    
                case 'Í': case 'Ì': case 'Î':
                    aux[i] = 'I';
                    break;
                    
                case 'Õ': case 'Ó': case 'Ò': case 'Ô':
                    aux[i] = 'O';
                    break;
                    
                case 'Ú': case 'Ù': case 'Û':
                    aux[i] = 'U';
                    break;
            }
        }
        return new String (aux);
    }
    
}
