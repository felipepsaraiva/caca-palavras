
package cacapalavras;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.Font;

public class GamePanel extends JPanel {
    //<editor-fold defaultstate="collapsed" desc="Declaration">
    public final static int maxLinhas = 10;
    public final static int maxColunas = 10;
    
    private JLabel[][] tabuleiro = new JLabel[maxLinhas][maxColunas];
    Word palavra;
    int inicio[] = new int[2];
    
    private final LblMouseListener mouseListener;
    private final Color defaultColor;
    private final Font defaultFont;
    //</editor-fold>
    
    public GamePanel(Word palavra) {
        //Inicializando Panel
        setLayout(new GridLayout(maxLinhas, maxColunas));
        mouseListener = new LblMouseListener();
        defaultFont = new Font("Tahoma", Font.BOLD, 24);
        defaultColor = new Color(240, 240, 240);
        this.palavra = palavra;
        //Inicializando Labels do Tabuleiro
        for(int i=0 ; i<maxLinhas ; i++) {
            for(int j=0 ; j<maxColunas ; j++) {
                tabuleiro[i][j] = new JLabel("");
                tabuleiro[i][j].setHorizontalAlignment(SwingConstants.CENTER);
                tabuleiro[i][j].setEnabled(false);
                tabuleiro[i][j].setOpaque(true);
                tabuleiro[i][j].setFont(defaultFont);
                tabuleiro[i][j].setBackground(defaultColor);
                tabuleiro[i][j].addMouseListener(mouseListener);
                add(tabuleiro[i][j]);
            }
        }
        
        preencherTabuleiro();
        posicionarPalavra();
    }
    
    private void preencherTabuleiro() {
        Random aleatorio = new Random();
        for(int i=0 ; i<maxLinhas ; i++) {
            for(int j=0 ; j<maxColunas ; j++) {
                tabuleiro[i][j].setText(String.valueOf( (char)(aleatorio.nextInt(26) + 65) ));
            }
        }
    }
    
    private void posicionarPalavra() {
        Random aleatorio = new Random();
        int borda = palavra.getTamanho();
        if (palavra.getTamanho() == 10)
            borda = 9;
        //Definir o Inicio e Desenhar a Palavra
        switch (palavra.getOrientacao()) {
            //Caso seja Horizontal
            case 0:
                inicio[0] = aleatorio.nextInt(maxLinhas);
                inicio[1] = aleatorio.nextInt(maxColunas-borda);
                for(int i=0 ; i<palavra.getTamanho() ; i++)
                    tabuleiro[inicio[0]][inicio[1]+i].setText( String.valueOf(palavra.toString().charAt(i)) );
                break;
            //Caso seja Vertical
            case 1:
                inicio[0] = aleatorio.nextInt(maxLinhas-borda);
                inicio[1] = aleatorio.nextInt(maxColunas);
                for(int i=0 ; i<palavra.getTamanho() ; i++)
                    tabuleiro[inicio[0]+i][inicio[1]].setText( String.valueOf(palavra.toString().charAt(i)) );
                break;
            //Caso seja Diagonal
            case 2:
                inicio[0] = aleatorio.nextInt(maxLinhas-borda);
                inicio[1] = aleatorio.nextInt(maxColunas-borda);
                for(int i=0 ; i<palavra.getTamanho() ; i++)
                    tabuleiro[inicio[0]+i][inicio[1]+i].setText( String.valueOf(palavra.toString().charAt(i)) );
                break;
            default:
                inicio[0] = 0;
                inicio[1] = 0;
        }
        
    }
    
    public boolean checkWord() {
        //Verifica se a palavra está selecionada
        boolean won;
        switch (palavra.getOrientacao()) {
            case 0:
                //Verifica se todas as Labels da palavra estão Enabled(true)
                for (int i=0 ; i<palavra.getTamanho() ; i++)
                    if (!tabuleiro[inicio[0]][inicio[1]+i].isEnabled())
                        return false;
                //Muda as Labels da palavra para Enabled(false) para verificar se existe alguma outra marcada
                for (int i=0 ; i<palavra.getTamanho() ; i++)
                    tabuleiro[inicio[0]][inicio[1]+i].setEnabled(false);
                won = checkOnly();
                //Se nao ganhou, retorna as Labels da palavra para Enabled(true)
                if (!won)
                    for (int i=0 ; i<palavra.getTamanho() ; i++)
                        tabuleiro[inicio[0]][inicio[1]+i].setEnabled(true);
                break;
            
            case 1:
                for (int i=0 ; i<palavra.getTamanho() ; i++)
                    if (!tabuleiro[inicio[0]+i][inicio[1]].isEnabled())
                        return false;
                for (int i=0 ; i<palavra.getTamanho() ; i++)
                    tabuleiro[inicio[0]+i][inicio[1]].setEnabled(false);
                won = checkOnly();
                if (!won)
                    for (int i=0 ; i<palavra.getTamanho() ; i++)
                        tabuleiro[inicio[0]+i][inicio[1]].setEnabled(true);
                break;
                
            case 2:
                for (int i=0 ; i<palavra.getTamanho() ; i++)
                    if (!tabuleiro[inicio[0]+i][inicio[1]+i].isEnabled())
                        return false;
                for (int i=0 ; i<palavra.getTamanho() ; i++)
                    tabuleiro[inicio[0]+i][inicio[1]+i].setEnabled(false);
                won = checkOnly();
                if (!won)
                    for (int i=0 ; i<palavra.getTamanho() ; i++)
                        tabuleiro[inicio[0]+i][inicio[1]+i].setEnabled(true);
                break;
               
            default:
                return false;
        }
        return won;
    }
    
    private boolean checkOnly() {
        //Verifica se APENAS a palavra está selecionada
        /*OBS: Na função checkWord(), após verificar se toda a palavra está selecionada, ele muda as Labels
        para Enabled(false), entao faz sentido verificar se exite alguma outra que está Enabled(true) */
        for (int i=0 ; i<maxLinhas ; i++) {
            for (int j=0 ; j<maxColunas ; j++) {
                if (tabuleiro[i][j].isEnabled())
                    return false;
            }
        }
        return true;
    }
    
    public void mostrarPalavra() {
        switch (palavra.getOrientacao()) {
            //Caso seja Horizontal
            case 0:
                for(int i=0 ; i<palavra.getTamanho() ; i++) {
                    tabuleiro[inicio[0]][inicio[1]+i].setEnabled(true);
                    tabuleiro[inicio[0]][inicio[1]+i].setBackground(Color.ORANGE);
                }
                break;
            //Caso seja Vertical
            case 1:
                for(int i=0 ; i<palavra.getTamanho() ; i++) {
                    tabuleiro[inicio[0]+i][inicio[1]].setEnabled(true);
                    tabuleiro[inicio[0]+i][inicio[1]].setBackground(Color.ORANGE);
                }
                break;
            //Caso seja Diagonal
            case 2:
                for(int i=0 ; i<palavra.getTamanho() ; i++) {
                    tabuleiro[inicio[0]+i][inicio[1]+i].setEnabled(true);
                    tabuleiro[inicio[0]+i][inicio[1]+i].setBackground(Color.ORANGE);
                }
                break;
        }
    }
    
    public class LblMouseListener implements MouseListener {
        @Override
        public void mouseClicked(MouseEvent e) {
            JLabel ref = (JLabel)e.getSource();
            if (ref.isEnabled()) {
                ref.setEnabled(false);
                ref.setBackground(defaultColor);
            } else {
                ref.setEnabled(true);
                ref.setBackground(Color.CYAN);
            }
            if (checkWord())
                Principal.getFrmWindow().gameWon();
        }
        
        @Override
        public void mousePressed(MouseEvent e) {}
        @Override
        public void mouseReleased(MouseEvent e) {}
        @Override
        public void mouseEntered(MouseEvent e) {}
        @Override
        public void mouseExited(MouseEvent e) {}
    }
    
}
